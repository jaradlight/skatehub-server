<?php

class Challenge extends Eloquent {
    
    protected $fillable = array('name','description','latitude','longitude','owner');

    protected $visible = array('name','latitude','longitude','photopath');

    public static $rules = array(
        'name'=>'required',
		'description'=>'required',
		'latitude'=>'required',
		'longitude'=>'required',
        'owner'=>'required|numeric'
	);

	public static function validate($data){
		return Validator::make($data, static::$rules);
	}
    
}