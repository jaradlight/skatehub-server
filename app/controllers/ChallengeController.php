<?php

class ChallengeController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(){
		$validation = Challenge::validate(Input::all());

		if($validation->fails()){
			return Redirect::to('/')
				->withErrors($validation)
				->with('message','Challenge creation failed.')
				->withInput();
		} else {

			$challenge = Challenge::create(array(
				'name'=>Input::get('name'),
				'description'=>Input::get('description'),
				'latitude'=>Input::get('latitude'),
				'longitude'=>Input::get('longitude'),
				'owner'=>Auth::user()->id,
			));

			$file = Input::file('photo'); // your file upload input field in the form should be named 'file'

			$newName = uniqid();

			$destinationPath = 'uploads/';
			$filename = $file->getClientOriginalName();
			$extension =$file->getClientOriginalExtension(); //if you need extension of the file
			$uploadSuccess = Input::file('photo')->move($destinationPath, $newName.'.'.$extension);
			
			$challenge->photopath = $destinationPath.$newName.'.'.$extension;
			$challenge->save();
			
			return Redirect::to('/')
				->with('message', 'Challenge created.');
			
			return "Success!";
		}
	}

	public function add(){

		$challenge = Challenge::create(array(
			'name'=>Input::get('name'),
			'description'=>'default',//Input::get('description'),
			'latitude'=>Input::get('latitude'),
			'longitude'=>Input::get('longitude'),
			'owner'=>1
		));

		$file = Input::file('photo'); // your file upload input field in the form should be named 'file'

		$newName = uniqid();

		$destinationPath = 'uploads/';
		$filename = $file->getClientOriginalName();
		$extension =$file->getClientOriginalExtension(); //if you need extension of the file
		$uploadSuccess = Input::file('photo')->move($destinationPath, $newName.'.'.$extension);
		
		$challenge->photopath = $destinationPath.$newName.'.'.$extension;
		$challenge->save();

		return Redirect::to('/');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function single($id){
		return View::make('challenges.single')
			->with('challenge',Challenge::find($id));
	}

}