<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function signup(){
		$validation = User::validate(Input::all());

		if($validation->fails()){
			return Redirect::to('/')
				->withErrors($validation)
				->with('message','Account creation failed.')
				->withInput();
		} else {

			User::create(array(
				'username'=>strtolower(Input::get('username')),
				'password'=>Hash::make(Input::get('password')),
				'email'=>Input::get('email')
			));

			return Redirect::to('/')
				->with('message', 'Account created.');
		}
	}

	public function login(){
		$userdata = array(
			'username' => Input::get('username'),
			'password' => Input::get('password')
		);

		if(Auth::attempt($userdata)){
			return Redirect::to('/');
		}

		return Redirect::to('/')
			->with('login_errors', true)
			->with('message','Login failed.');
	}

	public function logout(){
		Auth::logout();
		return Redirect::to('/');
		//return "whelp.";
	}

	public function harro(){
		return "Fart noise.";
	}
}