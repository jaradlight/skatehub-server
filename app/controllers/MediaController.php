<?php

class MediaController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

    public function create(){
        $validation = Media::validate(Input::all());

		if($validation->fails()){
		    return Redirect::to('/')
				->withErrors($validation)
				->with('message','Media item creation failed.')
				->withInput();
		} else {


            $file = Input::file('file'); // your file upload input field in the form should be named 'file'
            
            $destinationPath = 'uploads/'.str_random(8);
            $filename = $file->getClientOriginalName();
            //$extension =$file->getClientOriginalExtension(); //if you need extension of the file
            $uploadSuccess = Input::file('file')->move($destinationPath, $filename);



			Media::create(array(
				'title'=>Input::get('title'),
				'description'=>Input::get('description'),
                'filepath'=> $destinationPath.'/'.$filename
			));

			return Redirect::to('/')
				->with('message', 'Media saved.');
		}
	}
    
    private static function storeFile($file){
        // your file upload input field in the form should be named 'file'

        $destinationPath = 'uploads/'.str_random(8);
        //$filename = $file->getClientOriginalName();
        $filename = str_random(16);
        //$extension =$file->getClientOriginalExtension(); //if you need extension of the file
        $uploadSuccess = $file->move($destinationPath, $filename);
        
        //return $uploadSuccess;
        return $destinationPath.$filename;
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}