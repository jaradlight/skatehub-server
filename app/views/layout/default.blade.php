<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document Title</title>
	{{ HTML::style('css/style.css'); }}
</head>
<body>
    <div class="wrapper">
	    @yield('content')
	</div>
</body>
</html>