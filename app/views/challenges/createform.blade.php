@if(Auth::check())
<div class="panel">
    <h2>Create a Location</h2>

	{{ Form::open(array('url' => 'challenge/create','files' => true)) }}

	<ul class="errors">
	@foreach($errors->all() as $message)
		<li>{{ $message }}</li>
	@endforeach
	</ul>
	
	<table>
	<tr>
		<td>{{ Form::label('name','Name:') }}</td>
		<td>{{ Form::text('name') }}</td>
	</tr>
	<tr>
		<td>{{ Form::label('description','Description:') }}</td>
		<td>{{ Form::text('description') }}</td>
	</tr>
    
	

	<tr>
		<td>{{ Form::label('latitude','Latitude:') }}</td>
		<td>{{ Form::text('latitude', null, array('class'=>'js-lat')) }}</td>
	</tr>
	<tr>
		<td>{{ Form::label('longitude','Longitude:') }}</td>
		<td>{{ Form::text('longitude', null, array('class'=>'js-lon')) }}</td>
	</tr>

	<tr>
		<td>{{ Form::file('photo'); }}</td>
	</tr>

	<tr>
    <td><a class="button" style="background:lightgrey;padding: 5px; border: 1px solid #111; cursor:pointer; " onclick="getLocation()">Add My Coordinates</a></td>
    </tr>
	</table>
	
	{{ Form::hidden('owner', Auth::user()->id) }}

	{{ Form::submit('Create', array('class'=>'button')) }}

	{{ Form::close() }}
    
    
    <script>
        var lat=document.getElementsByClassName("js-lat")[0];
        var lon=document.getElementsByClassName("js-lon")[0];
        
        function getLocation()
          {
          if (navigator.geolocation)
            {
            navigator.geolocation.getCurrentPosition(showPosition);
            }
          //else{x.innerHTML="Geolocation is not supported by this browser.";}
          }
        function showPosition(position)
          {
          lat.value=position.coords.latitude;
          lon.value=position.coords.longitude;    
          }
    </script>

</div>
@endif