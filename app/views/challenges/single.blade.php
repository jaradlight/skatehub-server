@extends('layout.default')

@section('content')

<h1 class="js-title">{{ $challenge->name }}</h1>

<p>{{ $challenge->description }}</p>

<table>
	<tr>
		<td>Latitude</td>
		<td>Longitude</td>
	</tr>
	<tr>
		<td class="js-lat">{{ $challenge->latitude }}</td>
		<td class="js-lon">{{ $challenge->longitude }}</td>
	</tr>
</table>

{{ asset($challenge->photopath)}}
<?php $url = asset($challenge->photopath); ?>
<img src="{{ $url }}" alt="">

<style>
      #map-canvas {
        margin: 0;
        padding: 0;
        height: 400px;
      }
</style>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script>
    function initialize() {

        //Grab the lat and lon values from the page.
        var lat = document.getElementsByClassName("js-lat")[0].innerHTML;
        var lon = document.getElementsByClassName("js-lon")[0].innerHTML;
    
        var position = new google.maps.LatLng(lat,lon);
        var mapOptions = {
            zoom: 18,
            center: position, // center the map on the give coordinates
            mapTypeId: google.maps.MapTypeId.SATELLITE
        }
        var map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
            
        var marker = new google.maps.Marker({
            position: position,
            map: map,
            title: document.getElementsByClassName('js-title')[0].innerHTML
        });      
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>

<div id="map-canvas"></div>

@stop