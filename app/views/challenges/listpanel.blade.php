<div class="panel">
	<h2>Locations</h2>
	<ul class="challenges">
	@foreach($challenges as $c)
		<li>{{ HTML::linkRoute('challengeSingle',$c->name,array('id'=>$c->id)) }}</li>
	@endforeach
	</ul>
</div>