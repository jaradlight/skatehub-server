@extends('layout.default')
@section('content')

@if(Auth::guest())

	@include('users.signup')

	</div>
		<h2>Sign In</h2>
		{{ Form::open(array('url'=>'login','method'=>'post')) }}
			{{ Form::text('username', null, array('placeholder'=>'username')) }}
			{{ Form::password('password', array('placeholder'=>'password')) }}
		{{ Form::submit('Log In', array('class'=>'button')) }}
		{{ Form::close() }}
	</div>

@endif

@include('challenges.createform')

@include('challenges.listpanel')

@stop