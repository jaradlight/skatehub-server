<?php

use Illuminate\Database\Migrations\Migration;

class CreateChallengesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	    Schema::create('challenges', function($table){
    		$table->increments('id');
			$table->string('name');
			$table->text('description');
			$table->decimal('latitude',20,16);
			$table->decimal('longitude',20,16);

			$table->string('photopath');

			$table->integer('owner')->unsigned();
			$table->foreign('owner')
				  ->references('id')->on('users');
            
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.php artisan migrate:reset
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('challenges');
	}

}